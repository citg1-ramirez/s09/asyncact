package com.zuitt.AsyncAct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class AsyncActApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsyncActApplication.class, args);
	}

	ArrayList<Student> studentList = new ArrayList<Student>();

	@GetMapping("/welcome")
	public String welcome(@RequestParam(value="user", defaultValue="user") String user, @RequestParam(value="role", defaultValue = "role") String role){
		String message = "";
		if(role.equalsIgnoreCase("admin"))
			message = "Welcome back to the class portal, Admin " + user +"!";
		else if(role.equalsIgnoreCase("teacher"))
			message = "Thank you for logging in, Teacher " + user + "!";
		else if(role.equalsIgnoreCase("student"))
			message = "Welcome to the class portal, " + user + "!";
		else
			message = "Role out of range!";

		return message;
	}

	@GetMapping("/register")
	public String register (@RequestParam(value = "id", defaultValue = "0") int id, @RequestParam(value = "name", defaultValue = "name") String name, @RequestParam(value = "course", defaultValue = "course") String course){

		Student student = new Student(id, name, course);
		studentList.add(student);

		return String.format("%d your ID number is registered on the system!", student.getId());

	}


	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") int id){

		for(int i=0; i < studentList.size(); i++) {
			if (studentList.get(i).getId() == id) {
				return String.format("Welcome back %s! You are currently enrolled in %s.", studentList.get(i).getName(), studentList.get(i).getCourse());
			}
		}
			return String.format("Your provided %d is not found in the system!", id);


	}
}

